package main

import (
	"log"
	"os"
)

type twitterConfig struct {
	consumerKey    string
	consumerSecret string
	userKey        string
	userSecret     string
	newScreenName  string
}

func env() twitterConfig {
	f := func(e string) (v string) {
		v = os.Getenv(e)
		if v == "" {
			log.Fatal("missing env var: ", e)
		}

		return
	}

	tc := twitterConfig{}

	tc.consumerKey = f("CONSUMER_KEY")
	tc.consumerSecret = f("CONSUMER_SECRET")
	tc.userKey = f("USER_KEY")
	tc.userSecret = f("USER_SECRET")
	tc.newScreenName = f("NEW_SCREEN_NAME")

	return tc
}
