# `tns`: Twitter name sentinel

This small Twitter bot will check whether a Twitter username becomes free, and changes your account screen name to that.

## How-to

1. create a Twitter application consumer key/secret and action key/secret
2. export env vars
3. run `tns` and pray

## Required environment variables:

1. `CONSUMER_KEY`
2. `CONSUMER_SECRET`
3. `USER_KEY` (action key)
4. `USER_SECRET` (action secret)
5. `NEW_SCREEN_NAME` (the username you want to monitor)