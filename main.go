package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

const (
	screenNameURLFmt = "https://api.twitter.com/1.1/account/update_profile.json?screen_name=%s"

	delay = 90 * time.Second
)

func main() {
	tc := env()
	config := oauth1.NewConfig(tc.consumerKey, tc.consumerSecret)
	token := oauth1.NewToken(tc.userKey, tc.userSecret)
	httpClient := config.Client(oauth1.NoContext, token)

	client := twitter.NewClient(httpClient)

	for {
		log.Println("checking...")
		available, err := usernameAvailable(tc.newScreenName, client)
		if err != nil {
			log.Println(err)
		}

		if available {
			err = changeUsername(httpClient, tc.newScreenName)
			if err != nil {
				log.Println(err)
			}

			log.Println("done! goodbye!")
			return
		}

		log.Println("nothing, better luck next time")
		time.Sleep(delay)
	}
}

func usernameAvailable(username string, client *twitter.Client) (bool, error) {
	params := &twitter.UserShowParams{ScreenName: username}
	_, _, err := client.Users.Show(params)
	if err != nil && strings.Contains(err.Error(), "User not found") {
		return true, nil
	} else if err != nil && !strings.Contains(err.Error(), "User not found") {
		return false, err
	}

	return false, nil
}

func changeUsername(client *http.Client, username string) error {
	data, err := client.Post(
		fmt.Sprintf(screenNameURLFmt, username),
		"application/json",
		nil,
	)

	if err != nil {
		return err
	}

	var tr UpdateProfile
	jdec := json.NewDecoder(data.Body)
	err = jdec.Decode(&tr)
	if err != nil {
		return err
	}

	if !(tr.ScreenName == username) {
		return fmt.Errorf("something went wrong when changing username, check your twitter profile")
	}

	return nil
}
